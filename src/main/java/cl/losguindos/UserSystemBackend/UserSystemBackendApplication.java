package cl.losguindos.UserSystemBackend;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class UserSystemBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserSystemBackendApplication.class, args);
    }
}
